package ru.gryazev.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;

@Component
public class AboutCommand extends AbstractCommand {

    public AboutCommand() {
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Shows information about build.";
    }

    @Override
    public void execute() {
        if (terminalService == null) return;
        terminalService.print("Created by: " + Manifests.read("Created-By"));
        terminalService.print("Version: " + Manifests.read("buildNumber"));
    }

}
