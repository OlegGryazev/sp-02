package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.error.CrudInitializationException;

@Component
public final class ProjectCreateCommand extends AbstractProjectCommand {

    private IUserEndpoint userEndpoint;

    public ProjectCreateCommand(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        @NotNull final User currentUser = userEndpoint.findCurrentUser(token);
        if (currentUser == null) throw new CrudInitializationException();
        terminalService.print("[PROJECT CREATE]");
        @NotNull final Project project = terminalService.getProjectFromConsole();
        project.setUserId(currentUser.getId());
        @Nullable final Project createdProject = projectEndpoint.createProject(token, project);
        if (createdProject == null) throw new CrudCreateException();
        terminalService.print("[OK]");
    }

}
