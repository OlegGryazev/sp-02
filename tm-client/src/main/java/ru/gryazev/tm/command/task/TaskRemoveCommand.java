package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class TaskRemoveCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task from selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable final String projectId = getProjectId();
        @Nullable final String taskId = taskEndpoint.getTaskId(token, projectId, taskIndex);
        taskEndpoint.removeTask(token, taskId);
        terminalService.print("[DELETED]");
    }

}
