package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.util.List;

@Component
public class ProjectFindByDetails extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-find-details";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "List of projects by details or part of details.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        @NotNull final String projectDetails = terminalService.getSearchString();
        @NotNull final List<Project> projects = projectEndpoint.findProjectByDetails(token,projectDetails);
        if (projects.isEmpty()) throw new CrudListEmptyException();
        for (int i = 0; i < projects.size(); i++)
            terminalService.print((i + 1) + ". " + projects.get(i).getName());
    }

}
