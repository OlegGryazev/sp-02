package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.endpoint.ITaskEndpoint;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.error.CrudInitializationException;

@Component
public final class TaskCreateCommand extends AbstractTaskCommand {

    private IUserEndpoint userEndpoint;

    public TaskCreateCommand(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task at selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        @NotNull final User currentUser = userEndpoint.findCurrentUser(token);
        if (currentUser == null) throw new CrudInitializationException();
        terminalService.print("[TASK CREATE]");
        @NotNull final Task task = terminalService.getTaskFromConsole();
        @Nullable String currentProjectId = getProjectId();
        task.setUserId(currentUser.getId());
        task.setProjectId(currentProjectId);
        @Nullable final Task createdTask = taskEndpoint.createTask(token, task);
        if (createdTask == null) throw new CrudCreateException();
        terminalService.print("[OK]");
    }

}
