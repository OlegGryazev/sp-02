package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudInitializationException;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@Component
public final class ProjectEditCommand extends AbstractProjectCommand {

    private IUserEndpoint userEndpoint;

    public ProjectEditCommand(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        @NotNull final User currentUser = userEndpoint.findCurrentUser(token);
        if (currentUser == null) throw new CrudInitializationException();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectEndpoint.getProjectId(token, projectIndex);
        if (projectId == null) throw new CrudNotFoundException();

        @NotNull final Project project = terminalService.getProjectFromConsole();
        project.setUserId(currentUser.getId());
        project.setId(projectId);
        @Nullable final Project editedProject = projectEndpoint.editProject(token, project);
        if (editedProject == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
