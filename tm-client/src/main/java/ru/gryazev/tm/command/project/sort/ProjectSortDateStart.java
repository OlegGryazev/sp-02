package ru.gryazev.tm.command.project.sort;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.bean.StateBean;
import ru.gryazev.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public class ProjectSortDateStart extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sort-date-start";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts projects in order of date of project start.";
    }

    @Override
    public void execute() {
        if (terminalService == null) return;
        applicationContext.getBean(StateBean.class).getSettings().put("project-sort", "date-start");
        terminalService.print("[OK]");
    }

}
