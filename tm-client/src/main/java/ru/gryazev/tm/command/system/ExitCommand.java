package ru.gryazev.tm.command.system;

import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;

@Component
public final class ExitCommand extends AbstractCommand {

    public ExitCommand() {
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from program.";
    }

    @Override
    public void execute() {
        if (terminalService == null) return;
        terminalService.close();
        System.exit(0);
    }

}
