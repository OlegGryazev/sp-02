package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IProjectEndpoint;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.error.CrudNotFoundException;

@Component
public final class ProjectViewCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-view";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectEndpoint.getProjectId(token, projectIndex);

        @Nullable final Project project = projectEndpoint.findOneProject(token, projectId);
        if (project == null) throw new CrudNotFoundException();
        terminalService.printProject(project);
    }

}
