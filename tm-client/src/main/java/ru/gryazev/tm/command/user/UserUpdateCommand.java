package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@Component
@NoArgsConstructor
public final class UserUpdateCommand extends AbstractCommand {

    private IUserEndpoint userEndpoint;

    public UserUpdateCommand(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update user password.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @Nullable final String token = getToken();
        @Nullable final String pwd = terminalService.getPwdHashFromConsole();
        @Nullable final String pwdRepeat = terminalService.getPwdHashFromConsole();
        if (!pwd.equals(pwdRepeat)){
            terminalService.print("Entered passwords do not match!");
            return;
        }

        @Nullable final User user = userEndpoint.findCurrentUser(token);
        if (user == null) throw new CrudNotFoundException();
        user.setPwdHash(pwd);
        @Nullable final User editedUser = userEndpoint.editUser(token, user);
        if (editedUser == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
