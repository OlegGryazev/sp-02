package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IProjectEndpoint;

@NoArgsConstructor
public abstract class AbstractProjectCommand extends AbstractCommand {

    @Autowired
    protected IProjectEndpoint projectEndpoint;

}
