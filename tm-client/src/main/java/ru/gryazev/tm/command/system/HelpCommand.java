package ru.gryazev.tm.command.system;

import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.context.Bootstrap;

@Component
public final class HelpCommand extends AbstractCommand {

    public HelpCommand() {
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        applicationContext.getBean("bootstrap", Bootstrap.class).getCommands()
                .forEach((k, v) -> terminalService.print(v.getName() + ": " + v.getDescription()));;
    }

}
