package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.util.List;

@Component
public class ProjectFindByName extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-find-name";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "List of projects by name or part of name.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        @NotNull final String projectName = terminalService.getSearchString();
        @NotNull final List<Project> projects = projectEndpoint.findProjectByName(token,projectName);
        if (projects.isEmpty()) throw new CrudListEmptyException();
        for (int i = 0; i < projects.size(); i++)
            terminalService.print((i + 1) + ". " + projects.get(i).getName());
    }

}
