package ru.gryazev.tm.command.project.sort;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.bean.StateBean;
import ru.gryazev.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public class ProjectSortDefault extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sort-default";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts projects in order of creation.";
    }

    @Override
    public void execute() {
        if (terminalService == null) return;
        applicationContext.getBean(StateBean.class).getSettings().put("project-sort", "default");
        terminalService.print("[OK]");
    }

}
