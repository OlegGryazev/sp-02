package ru.gryazev.tm.command.project.sort;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.bean.StateBean;
import ru.gryazev.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public class ProjectSortDateFinish extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sort-date-finish";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts projects in order of date of project finish.";
    }

    @Override
    public void execute() {
        if (terminalService == null) return;
        applicationContext.getBean(StateBean.class).getSettings().put("project-sort", "date-finish");
        terminalService.print("[OK]");
    }

}
