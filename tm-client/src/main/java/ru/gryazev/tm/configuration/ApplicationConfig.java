package ru.gryazev.tm.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ru.gryazev.tm.api.context.SessionLocator;
import ru.gryazev.tm.api.service.ITerminalService;
import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.endpoint.*;
import ru.gryazev.tm.service.TerminalService;

import java.lang.Exception;
import java.net.URL;

@Configuration
@PropertySource("client.properties")
@ComponentScan(basePackages = "ru.gryazev.tm")
public class ApplicationConfig {

    @Value("${project}")
    private String projectUrlString;

    @Value("${task}")
    private String taskUrlString;

    @Value("${user}")
    private String userUrlString;

    @Value("${session}")
    private String sessionUrlString;

    @Value("${system}")
    private String systemUrlString;

    @Bean
    public ITerminalService terminalService() {
        return new TerminalService();
    }

    @Bean
    public Bootstrap bootstrap() {
        return new Bootstrap();
    }

    @Bean
    public SessionLocator sessionLocator() {
        return new Bootstrap();
    }

    @Bean
    public IProjectEndpoint projectEndpoint() throws Exception {
        return new ProjectEndpointService(new URL(projectUrlString)).getProjectEndpointPort();
    }

    @Bean
    public ITaskEndpoint taskEndpoint() throws Exception {
        return new TaskEndpointService(new URL(taskUrlString)).getTaskEndpointPort();
    }

    @Bean
    public IUserEndpoint userEndpoint() throws Exception {
        return new UserEndpointService(new URL(userUrlString)).getUserEndpointPort();
    }

    @Bean
    public ISessionEndpoint sessionEndpoint() throws Exception {
        return new SessionEndpointService(new URL(sessionUrlString)).getSessionEndpointPort();
    }

    @Bean
    public ISystemEndpoint systemEndpoint() throws Exception {
        return new SystemEndpointService(new URL(systemUrlString)).getSystemEndpointPort();
    }

}
