package ru.gryazev.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.gryazev.tm.context.Bootstrap;

import javax.jms.*;

@Configuration
@ComponentScan("ru.gryazev.tm")
public class ApplicationConfig {

    @Bean
    public ActiveMQConnectionFactory activeMQConnectionFactory() {
        return new ActiveMQConnectionFactory("tcp://localhost:61616");
    }

    @Bean
    public MessageConsumer consumer(@NotNull final ActiveMQConnectionFactory activeMQConnectionFactory) throws JMSException {
        @NotNull final Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic("CRUDLOGS");
        return session.createConsumer(destination);
    }

}
