package ru.gryazev.tm.api;

import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.io.IOException;

public interface ILoggingService {

    public void writeLog(@NotNull TextMessage textMessage) throws IOException, JMSException;

}
