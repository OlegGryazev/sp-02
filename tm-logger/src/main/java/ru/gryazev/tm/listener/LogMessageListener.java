package ru.gryazev.tm.listener;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.api.ILoggingService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public class LogMessageListener implements MessageListener {

    @Autowired
    private ILoggingService loggingService;

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            loggingService.writeLog(((TextMessage) message));
        }
    }

}
