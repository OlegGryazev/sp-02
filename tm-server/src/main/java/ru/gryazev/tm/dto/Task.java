package ru.gryazev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractCrudDTO implements ComparableEntity {

    @Nullable
    private String userId = null;

    @Nullable
    private String projectId = null;

    @Nullable
    private String name = "";

    @Nullable
    private String details = "";

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private Long createMillis = new Date().getTime();

    @Nullable
    public static TaskEntity toTaskEntity(@NotNull final ApplicationContext context, @Nullable final Task task) {
        if (task == null) return null;
        @Nullable final UserEntity userEntity = context.getBean("userService", IUserService.class).findOne(task.getUserId());
        if (userEntity == null) return null;
        @NotNull final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(task.getId());
        taskEntity.setProject(context.getBean("projectService", IProjectService.class).findOne(task.getUserId(), task.getProjectId()));
        taskEntity.setUser(userEntity);
        taskEntity.setName(task.getName());
        taskEntity.setDetails(task.getDetails());
        taskEntity.setStatus(task.getStatus());
        taskEntity.setDateStart(task.getDateStart());
        taskEntity.setDateFinish(task.getDateFinish());
        taskEntity.setCreateMillis(task.getCreateMillis());
        return taskEntity;
    }

}
