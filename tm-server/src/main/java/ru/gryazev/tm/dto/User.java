package ru.gryazev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ISessionService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractCrudDTO {

    @Nullable
    private String login = "";

    @Nullable
    private String pwdHash = "";

    @Nullable
    private RoleType roleType;

    @Nullable
    private String name;

    @NotNull
    @Override
    @JsonIgnore
    public String getUserId() {
        return getId();
    }

    @Nullable
    public static UserEntity toUserEntity(@NotNull final ApplicationContext context, @Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserEntity userEntity = new UserEntity();
        userEntity.setId(user.getId());
        userEntity.setLogin(user.getLogin());
        userEntity.setPwdHash(user.getPwdHash());
        userEntity.setName(user.getName());
        userEntity.setRoleType(user.getRoleType());
        userEntity.setProjects(context.getBean("projectService", IProjectService.class).findByUserId(user.getId()));
        userEntity.setTasks(context.getBean("taskService", ITaskService.class).findByUserId(user.getId()));
        userEntity.setSessions(context.getBean("sessionService", ISessionService.class).findByUserId(user.getId()));
        return userEntity;
    }

}
