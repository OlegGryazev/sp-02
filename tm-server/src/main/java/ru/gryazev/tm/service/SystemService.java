package ru.gryazev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.gryazev.tm.api.service.ISystemService;
import ru.gryazev.tm.constant.Constant;

@Service
@NoArgsConstructor
public class SystemService implements ISystemService {

    @Nullable
    public String getServerInformation() {
        return String.format("%s : %s", Constant.HOST, Constant.PORT);
    }

}
