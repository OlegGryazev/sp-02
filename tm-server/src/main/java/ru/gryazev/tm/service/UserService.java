package ru.gryazev.tm.service;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserService implements IUserService {

    private final IUserRepository userRepository;

    @Autowired
    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    public UserEntity create(@Nullable final UserEntity userEntity) {
        if (!isEntityValid(userEntity)) return null;
        return userRepository.save(userEntity);
    }

    @Nullable
    @Override
    public String getUserId(int userIndex) {
        if (userIndex < 0) return null;
        @NotNull final List<UserEntity> userEntities = new ArrayList<>();
        userRepository.findAll().forEach(userEntities::add);
        return userIndex >= userEntities.size() ? null : userEntities.get(userIndex).getId();
    }

    @Contract("null -> false")
    public boolean isEntityValid(@Nullable final UserEntity userEntity) {
        if (userEntity == null || userEntity.getRoleType() == null) return false;
        if (userEntity.getId().isEmpty()) return false;
        if (userEntity.getLogin() == null || userEntity.getLogin().isEmpty()) return false;
        return userEntity.getPwdHash() != null && !userEntity.getPwdHash().isEmpty();
    }

    @Nullable
    @Override
    public UserEntity findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return userRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public UserEntity edit(@Nullable final String userId, @Nullable final UserEntity userEntity) {
        if (!isEntityValid(userEntity)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(userEntity.getId())) return null;
        if (!userRepository.findById(userEntity.getId()).isPresent()) return null;
        return userRepository.save(userEntity);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        userRepository.deleteById(id);
    }

    @Override
    public void removeAll() {
        userRepository.deleteAll();
    }

    @NotNull
    @Override
    public List<UserEntity> findAll() {
        @NotNull final List<UserEntity> userEntities = new ArrayList<>();
        userRepository.findAll().forEach(userEntities::add);
        return userEntities;
    }

}
