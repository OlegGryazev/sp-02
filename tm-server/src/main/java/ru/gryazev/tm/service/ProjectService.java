package ru.gryazev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.entity.ProjectEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    @Autowired
    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Nullable
    public String getProjectId(@Nullable final String userId, final int projectIndex) {
        if (userId == null || userId.isEmpty() || projectIndex < 0) return null;
        @Nullable final List<ProjectEntity> projectEntities = new ArrayList<>();
        projectRepository.findByUserId(userId).forEach(projectEntities::add);
        return projectIndex >= projectEntities.size() ? null : projectEntities.get(projectIndex).getId();
    }

    @Nullable
    @Override
    public ProjectEntity create(@Nullable final String userId, @Nullable final ProjectEntity projectEntity) {
        if (userId == null || userId.isEmpty()) return null;
        if (!isEntityValid(projectEntity)) return null;
        if (!userId.equals(projectEntity.getUser().getId())) return null;
        return projectRepository.save(projectEntity);
    }

    @Nullable
    @Override
    public ProjectEntity edit(@Nullable final String userId, @Nullable final ProjectEntity projectEntity) {
        if (!isEntityValid(projectEntity)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(projectEntity.getUser().getId())) return null;
        if (!projectRepository.findById(projectEntity.getId()).isPresent()) return null;
        return projectRepository.save(projectEntity);
    }

    @Nullable
    @Override
    public ProjectEntity findOne(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.findByUserIdAndId(userId, projectId).orElse(null);
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByName(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectEntity> projects = new ArrayList<>();
        projectRepository.findByUserIdAndNameLike(userId, "%" + projectName + "%").forEach(projects::add);
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByDetails(@Nullable final String userId, @Nullable final String projectDetails) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectDetails == null || projectDetails.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectEntity> projects = new ArrayList<>();
        projectRepository.findByUserIdAndDetailsLike(userId, "%" + projectDetails + "%").forEach(projects::add);
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectEntity> projects = new ArrayList<>();
        projectRepository.findByUserId(userId).forEach(projects::add);
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByUserIdSorted(@Nullable final String userId, @Nullable String sortType) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortType == null) sortType = "default";
        final Iterable<ProjectEntity> searchResult;
        switch (sortType) {
            case "date-start": searchResult = projectRepository.findByUserIdOrderByDateStart(userId);
                break;
            case "date-finish": searchResult = projectRepository.findByUserIdOrderByDateFinish(userId);
                break;
            case "status": searchResult = projectRepository.findByUserIdOrderByStatus(userId);
                break;
            default: searchResult = projectRepository.findByUserId(userId);
        }
        @NotNull final List<ProjectEntity> projects = new ArrayList<>();
        searchResult.forEach(projects::add);
        return projects;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.deleteByUserIdAndId(userId, projectId);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.deleteByUserId(userId);
    }

    @Override
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @NotNull
    @Override
    public List<ProjectEntity> findAll() {
        @NotNull final List<ProjectEntity> projects = new ArrayList<>();
        projectRepository.findAll().forEach(projects::add);
        return projects;
    }

    private boolean isEntityValid(@Nullable final ProjectEntity projectEntity) {
        if (projectEntity == null) return false;
        if (projectEntity.getId() == null || projectEntity.getId().isEmpty()) return false;
        if (projectEntity.getName() == null || projectEntity.getName().isEmpty()) return false;
        return projectEntity.getUser().getId() != null && !projectEntity.getUser().getId().isEmpty();
    }

}
