package ru.gryazev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.entity.TaskEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    @Autowired
    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    public TaskEntity create(@Nullable final String userId, @Nullable final TaskEntity task) {
        if (userId == null || userId.isEmpty()) return null;
        if (!isEntityValid(task)) return null;
        if (!userId.equals(task.getUser().getId())) return null;
        return taskRepository.save(task);
    }

    @Nullable
    @Override
    public TaskEntity edit(@Nullable final String userId, @Nullable final TaskEntity task) {
        if (!isEntityValid(task)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(task.getUser().getId())) return null;
        if (!taskRepository.findById(task.getId()).isPresent()) return null;
        return taskRepository.save(task);
    }

    @Nullable
    @Override
    public TaskEntity findOne(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.findByUserIdAndId(userId, taskId).orElse(null);
    }

    @NotNull
    @Override
    public List<TaskEntity> listTaskUnlinked(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskEntity> tasks = new ArrayList<>();
        taskRepository.findByUserIdAndProjectId(userId, null).forEach(tasks::add);
        return tasks;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        taskRepository.deleteByUserIdAndId(userId, taskId);
    }

    @Override
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.deleteByUserIdAndProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public String getTaskId(@Nullable final String userId, @Nullable final String projectId, final int taskIndex) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskIndex < 0) return null;
        @Nullable final List<TaskEntity> tasks = findByProjectId(userId, projectId);
        if (taskIndex >= tasks.size()) return null;
        return tasks.get(taskIndex).getId();
    }

    @NotNull
    @Override
    public List<TaskEntity> findByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskEntity> tasks = new ArrayList<>();
        taskRepository.findByUserIdAndProjectId(userId, projectId).forEach(tasks::add);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> findByName(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskName == null || taskName.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskEntity> tasks = new ArrayList<>();
        taskRepository.findByUserIdAndNameLike(userId, "%" + taskName + "%").forEach(tasks::add);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> findByDetails(@Nullable final String userId, @Nullable final String taskDetails) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskDetails == null || taskDetails.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskEntity> tasks = new ArrayList<>();
        taskRepository.findByUserIdAndDetailsLike(userId, "%" + taskDetails + "%").forEach(tasks::add);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> listTaskByProjectSorted(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String sortType
    ) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortType == null) sortType = "default";
        @NotNull final List<TaskEntity> tasks = new ArrayList<>();
        @NotNull final Iterable<TaskEntity> searchResult;
        switch (sortType) {
            case "date-start": searchResult = taskRepository.findByUserIdAndProjectIdOrderByDateStart(userId, projectId);
                break;
            case "date-finish": searchResult = taskRepository.findByUserIdAndProjectIdOrderByDateFinish(userId, projectId);
                break;
            case "status": searchResult = taskRepository.findByUserIdAndProjectIdOrderByStatus(userId, projectId);
                break;
            default: searchResult = taskRepository.findByUserIdAndProjectId(userId, projectId);
        }
        searchResult.forEach(tasks::add);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskEntity> tasks = new ArrayList<>();
        taskRepository.findByUserId(userId).forEach(tasks::add);
        return tasks;
    }

    @Override
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @NotNull
    @Override
    public List<TaskEntity> findAll() {
        @NotNull final List<TaskEntity> tasks = new ArrayList<>();
        taskRepository.findAll().forEach(tasks::add);
        return tasks;
    }

    public boolean isEntityValid(@Nullable final TaskEntity task) {
        if (task == null) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        if (task.getUser() == null || task.getUser().getId() == null || task.getUser().getId().isEmpty()) return false;
        return (task.getName() != null && !task.getName().isEmpty());
    }

}