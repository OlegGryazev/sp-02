package ru.gryazev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.gryazev.tm.api.service.IMessageProducerService;

import javax.jms.*;

@Service
public class MessageProducerService implements IMessageProducerService {

    private final ApplicationContext context;

    @Autowired
    public MessageProducerService(final ApplicationContext context) {
        this.context = context;
    }

    public void sendMessage(@NotNull final String message) throws JMSException {
        @NotNull final Connection connection = context.getBean("activeMQConnection", Connection.class);
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic("CRUDLOGS");
        @NotNull final MessageProducer producer = session.createProducer(destination);

        @NotNull final TextMessage textMessage = session.createTextMessage();
        textMessage.setText(message);
        producer.send(textMessage);
    }

}
