package ru.gryazev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gryazev.tm.api.repository.ISessionRepository;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.ISessionService;
import ru.gryazev.tm.constant.Constant;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.entity.DtoFactory;
import ru.gryazev.tm.entity.SessionEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.util.CryptUtil;
import ru.gryazev.tm.util.SignatureUtil;

import java.util.*;

@Service
@Transactional
@PropertySource("server.properties")
public class SessionService implements ISessionService {

    private final ISessionRepository sessionRepository;

    private final IUserRepository userRepository;

    @Value("${key}")
    private String key;

    @Autowired
    public SessionService(final ISessionRepository sessionRepository, final IUserRepository userRepository) {
        this.sessionRepository = sessionRepository;
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    public String create(@Nullable final String login, @Nullable final String pwdHash) throws Exception {
        if (login == null || login.isEmpty()) return null;
        if (pwdHash == null || pwdHash.isEmpty()) return null;
        @Nullable final UserEntity userEntity = userRepository.findByLoginAndPwdHash(login, pwdHash);
        if (userEntity == null) return null;
        @NotNull final SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setUser(userEntity);
        sessionEntity.setRole(userEntity.getRoleType());
        sessionEntity.setSignature(SignatureUtil.sign(DtoFactory.convertEntityToDto(sessionEntity), Constant.SALT, Constant.CYCLE_COUNT));
        sessionRepository.save(sessionEntity);
        return encryptSession(sessionEntity);
    }

    @Nullable
    private SessionEntity findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return sessionRepository.findById(id).orElse(null);
    }

    @NotNull
    @Override
    public List<SessionEntity> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @Nullable final List<SessionEntity> sessionEntities = new ArrayList<>();
        sessionRepository.findByUserId(userId).forEach(sessionEntities::add);
        return sessionEntities;
    }

    public void validateSession(@Nullable final Session session, @Nullable final RoleType[] roles) throws Exception {
        @NotNull final String message = "Session is not valid!";
        if (session == null) throw new Exception(message);
        if (roles == null || !Arrays.asList(roles).contains(session.getRole())) throw new Exception(message);
        validateSession(session);
    }

    public void validateSession(@Nullable final Session session) throws Exception {
        @NotNull final String message = "Session is not valid!";
        if (session == null) throw new Exception(message);
        if (session.getId().isEmpty()) throw new Exception(message);
        @NotNull final long sessionAliveTime = new Date().getTime() - session.getTimestamp();
        if (sessionAliveTime > Constant.SESSION_LIFETIME) throw new Exception(message);
        if (session.getRole() == null) throw new Exception(message);
        @Nullable final SessionEntity sessionAtServer = findOne(session.getId(), session.getId());
        if (sessionAtServer == null || sessionAtServer.getSignature() == null) throw new Exception(message);
        @Nullable final String actualSessionSignature =  SignatureUtil.sign(session, Constant.SALT, Constant.CYCLE_COUNT);
        if (!sessionAtServer.getSignature().equals(actualSessionSignature)) throw new Exception(message);
    }

    public void removeAll() {
        sessionRepository.deleteAll();

    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        sessionRepository.deleteById(id);
    }

    @Nullable
    private String encryptSession(@Nullable final SessionEntity sessionEntity) throws Exception {
        if (sessionEntity == null) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(DtoFactory.convertEntityToDto(sessionEntity));
        return CryptUtil.encrypt(json, key);
    }

}
