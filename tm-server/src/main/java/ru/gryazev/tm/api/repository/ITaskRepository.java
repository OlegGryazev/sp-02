package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gryazev.tm.entity.TaskEntity;

import java.util.Optional;

@Repository
public interface ITaskRepository extends CrudRepository<TaskEntity, String> {

    @NotNull
    public Optional<TaskEntity> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    public Iterable<TaskEntity> findByUserIdAndNameLike(@NotNull final String userId, @NotNull final String name);

    @NotNull
    public Iterable<TaskEntity> findByUserIdAndDetailsLike(@NotNull String userId, @NotNull String taskDetails);

    @NotNull
    public Iterable<TaskEntity> findByUserId(@NotNull String userId);

    @NotNull
    public Iterable<TaskEntity> findByUserIdAndProjectIdOrderByDateStart(
            @NotNull String userId,
            @Nullable String projectId
    );

    @NotNull
    public Iterable<TaskEntity> findByUserIdAndProjectIdOrderByDateFinish(
            @NotNull String userId,
            @Nullable String projectId
    );

    @NotNull
    public Iterable<TaskEntity> findByUserIdAndProjectIdOrderByStatus(
            @NotNull String userId,
            @Nullable String projectId
    );

    public void deleteByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    public void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    public Iterable<TaskEntity> findByUserIdAndProjectId(@NotNull String userId, @Nullable String projectId);

}
