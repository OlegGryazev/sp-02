package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.ProjectEntity;

import java.util.List;

public interface IProjectService {
   
    @Nullable
    public String getProjectId(@Nullable String userId, int projectIndex);

    @Nullable
    public ProjectEntity create(@Nullable String userId, @Nullable ProjectEntity projectEntity);

    @Nullable
    public ProjectEntity edit(@Nullable String userId, @Nullable ProjectEntity projectEntity) throws Exception;

    @Nullable
    public ProjectEntity findOne(@Nullable String userId, @Nullable String projectId);

    @NotNull
    public List<ProjectEntity> findByName(@Nullable String userId, @Nullable String projectName);

    @NotNull
    public List<ProjectEntity> findByDetails(@Nullable String userId, @Nullable String projectDetails);

    @NotNull
    public List<ProjectEntity> findByUserId(@Nullable String userId);

    @NotNull
    public List<ProjectEntity> findByUserIdSorted(@Nullable String userId, @Nullable String sortType);
    
    public void remove(@Nullable String userId, @Nullable String projectId);
    
    public void removeAll(@Nullable String userId);
    
    public void removeAll();

    @NotNull
    public List<ProjectEntity> findAll();
    
}
