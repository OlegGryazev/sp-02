package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gryazev.tm.entity.UserEntity;

@Repository
public interface IUserRepository extends CrudRepository<UserEntity, String> {

    public UserEntity findByLoginAndPwdHash(@NotNull String login, @NotNull String pwdHash);

}
