package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ISystemService {

    @Nullable
    public String getServerInformation() throws Exception;

}
