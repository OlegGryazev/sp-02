package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gryazev.tm.entity.ProjectEntity;

import java.util.Optional;

@Repository
public interface IProjectRepository extends CrudRepository<ProjectEntity, String> {
    
    @NotNull
    public Optional<ProjectEntity> findByUserIdAndId(@NotNull String userId, @NotNull String id);

    public void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    public void deleteByUserId(@NotNull String userId);

    @NotNull
    public Iterable<ProjectEntity> findByUserIdAndNameLike(@NotNull String userId, @NotNull String projectName);

    @NotNull
    public Iterable<ProjectEntity> findByUserIdAndDetailsLike(@NotNull String userId, @NotNull String projectDetails);

    @NotNull
    public Iterable<ProjectEntity> findByUserId(@NotNull String userId);

    @NotNull
    public Iterable<ProjectEntity> findByUserIdOrderByDateStart(@NotNull String userId);

    @NotNull
    public Iterable<ProjectEntity> findByUserIdOrderByDateFinish(@NotNull String userId);

    @NotNull
    public Iterable<ProjectEntity> findByUserIdOrderByStatus(@NotNull String userId);

}
