package ru.gryazev.tm.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gryazev.tm.entity.SessionEntity;

@Repository
public interface ISessionRepository extends CrudRepository<SessionEntity, String> {

    public Iterable<SessionEntity> findByUserId(final String userId);

}
