package ru.gryazev.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.jms.Connection;
import javax.jms.JMSException;

@Configuration
@Import({PersistenceJPAConfig.class})
@ComponentScan(basePackages = "ru.gryazev.tm")
public class ApplicationConfig {

    @Bean
    public ActiveMQConnectionFactory activeMQConnectionFactory() {
        return new ActiveMQConnectionFactory();
    }

    @Bean
    public Connection activeMQConnection(@NotNull final ActiveMQConnectionFactory activeMQConnectionFactory) throws JMSException {
        @NotNull final Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();
        return connection;
    }

}
