package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "app_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserEntity extends AbstractCrudEntity {

    @NotNull
    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<ProjectEntity> projects;

    @NotNull
    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<TaskEntity> tasks;

    @NotNull
    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<SessionEntity> sessions;

    @Nullable
    @Column(unique = true)
    private String login;

    @Nullable
    @Column(name = "pwd_hash")
    private String pwdHash;

    @Nullable
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private RoleType roleType;

    @Nullable
    private String name;

}
