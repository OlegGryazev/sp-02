package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.listener.AbstractCrudEntityListener;

import javax.persistence.Cacheable;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Setter
@Getter
@Cacheable
@MappedSuperclass
@EntityListeners(AbstractCrudEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public abstract class AbstractCrudEntity {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

}
