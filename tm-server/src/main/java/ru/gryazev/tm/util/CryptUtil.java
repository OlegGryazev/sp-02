package ru.gryazev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public final class CryptUtil {

    @Nullable
    public static String MD5(@Nullable final String md5) {
        if (md5 == null) return null;
        try {
            @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(md5.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (final byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @NotNull
    private static SecretKeySpec getKey(@NotNull final String myKey) throws Exception {
        @Nullable MessageDigest sha = null;
        @NotNull byte[] key = myKey.getBytes("UTF-8");
        sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);
        @NotNull final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
        return secretKey;
    }

    @NotNull
    public static String encrypt(@NotNull final String strToEncrypt, @NotNull final String secret)throws Exception {
        @NotNull final SecretKeySpec secretKey = getKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        @NotNull final byte[] data = cipher.doFinal(strToEncrypt.getBytes("UTF-8"));
        return Base64.getEncoder().encodeToString(data);
    }

    @NotNull
    public static String decrypt(@NotNull final String strToDecrypt, @NotNull final String secret)throws Exception {
        @NotNull final SecretKeySpec secretKey = getKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        @NotNull final byte[] data = Base64.getDecoder().decode(strToDecrypt);
        return new String(cipher.doFinal(data));
    }

}
