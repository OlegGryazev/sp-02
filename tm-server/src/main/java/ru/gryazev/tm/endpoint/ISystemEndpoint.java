package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.Nullable;

import javax.jws.WebService;

@WebService
public interface ISystemEndpoint {

    @Nullable
    public String getServerInformation(@Nullable String token) throws Exception;

}
