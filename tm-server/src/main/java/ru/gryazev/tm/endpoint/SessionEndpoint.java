package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.dto.Session;

import javax.jws.WebService;

@Component
@WebService(endpointInterface = "ru.gryazev.tm.endpoint.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Override
    public void removeSession(@Nullable final String token) throws Exception {
        if (token == null) return;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        sessionService.remove(session.getId());
    }

    @Nullable
    @Override
    public String createSession(@Nullable final String login, @Nullable final String pwdHash) throws Exception {
        return sessionService.create(login, pwdHash);
    }

}
