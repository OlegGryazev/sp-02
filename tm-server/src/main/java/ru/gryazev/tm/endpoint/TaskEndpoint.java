package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.dto.Task;
import ru.gryazev.tm.entity.DtoFactory;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@WebService(endpointInterface = "ru.gryazev.tm.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    private final ITaskService taskService;

    @Autowired
    public TaskEndpoint(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Nullable
    @Override
    public Task findOneTask(@Nullable final String token, @Nullable final String taskId) throws Exception {
        if (token == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @Nullable final TaskEntity taskEntity = taskService.findOne(session.getUserId(), taskId);
        return DtoFactory.convertEntityToDto(taskEntity);
    }

    @Nullable
    @Override
    public Task createTask(@Nullable final String token, @Nullable final Task task) throws Exception {
        if (token == null || task == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @Nullable final TaskEntity createdTask = taskService.create(session.getUserId(), Task.toTaskEntity(context, task));
        return DtoFactory.convertEntityToDto(createdTask);
    }

    @Override
    public void removeAllTask(@Nullable final String token) throws Exception {
        if (token == null) return;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session, new RoleType[]{RoleType.ADMIN});
        taskService.removeAll();
    }

    @Nullable
    @Override
    public Task editTask(@Nullable final String token, @Nullable final Task task) throws Exception {
        if (token == null || task == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @Nullable final TaskEntity editedTask = taskService.edit(session.getUserId(), Task.toTaskEntity(context, task));
        return DtoFactory.convertEntityToDto(editedTask);
    }

    @Override
    public void removeTask(@Nullable final String token, @Nullable final String taskId) throws Exception {
        if (token == null) return;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        taskService.remove(session.getUserId(), taskId);
    }

    @Nullable
    @Override
    public String getTaskId(@Nullable final String token, @Nullable final String projectId, final int taskIndex) throws Exception {
        if (token == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        return taskService.getTaskId(session.getUserId(), projectId, taskIndex);
    }

    @NotNull
    @Override
    public List<Task> findTaskByName(@Nullable final String token, @Nullable final String taskName) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @NotNull final List<TaskEntity> entities = taskService.findByName(session.getUserId(), taskName);
        @NotNull final List<Task> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(DtoFactory.convertEntityToDto(o)));
        return dtos;
    }

    @NotNull
    @Override
    public List<Task> findTaskByDetails(@Nullable final String token, @Nullable final String taskDetails) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @NotNull final List<TaskEntity> entities =  taskService.findByDetails(session.getUserId(), taskDetails);
        @NotNull final List<Task> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(DtoFactory.convertEntityToDto(o)));
        return dtos;
    }

    @Override
    public @NotNull List<Task> findTaskByProjectSorted(@Nullable String token, @Nullable String projectId, @Nullable String sortType) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @NotNull final List<TaskEntity> entities = taskService.listTaskByProjectSorted(session.getUserId(), projectId, sortType);
        @NotNull final List<Task> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(DtoFactory.convertEntityToDto(o)));
        return dtos;
    }

    @Override
    public @NotNull List<Task> listTaskUnlinkedSorted(@Nullable String token, @Nullable String sortType) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @NotNull final List<TaskEntity> entities = taskService.listTaskByProjectSorted(session.getUserId(),null, sortType);
        @NotNull final List<Task> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(DtoFactory.convertEntityToDto(o)));
        return dtos;
    }

    @NotNull
    @Override
    public List<Task> listTaskByProject(@Nullable final String token, @Nullable final String projectId) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @NotNull final List<TaskEntity> entities = taskService.findByProjectId(session.getUserId(), projectId);
        @NotNull final List<Task> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(DtoFactory.convertEntityToDto(o)));
        return dtos;
    }

    @NotNull
    @Override
    public  List<Task> listTaskUnlinked(@Nullable final String token) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @NotNull final List<TaskEntity> entities = taskService.listTaskUnlinked(session.getUserId());
        @NotNull final List<Task> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(DtoFactory.convertEntityToDto(o)));
        return dtos;
    }

    @Override
    public void removeByProjectId(@Nullable final String token, @Nullable final String projectId) throws Exception {
        if (token == null) return;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        taskService.removeByProjectId(session.getUserId(), projectId);
    }

}
