package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.api.service.ISystemService;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebService;

@Component
@WebService(endpointInterface = "ru.gryazev.tm.endpoint.ISystemEndpoint")
public class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @Nullable
    @Override
    public String getServerInformation(@Nullable final String token) throws Exception {
        if (token == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session, new RoleType[]{RoleType.ADMIN});
        return context.getBean("systemSerivce", ISystemService.class).getServerInformation();
    }

}
